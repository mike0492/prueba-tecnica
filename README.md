# Prueba Tecnica
¡Hola! Esta es mi solución para la Prueba Tecnica.

Programé la API REST para recibir los endpoints solicitados, usé MAVEN JAVA SPRING BOOT para la lógica.

Para la gestión de la BD, utilice MYSQL, El Script de la base de datos se encuentra dentro de este repositorio.

Estoy muy entusiasmado con esta oportunidad, hay elementos para mejorar en el código, pero estoy dispuesto a aprender más.


## BD
En este repositorio existe un archivo SQL con la estructura de la base de datos.

curseexam.sql

## BD Auto Generada
El proyecto tiene un comando en el archivo Aplication Propperties el cual crea la BD con toda su estructura y borra las tablas actuales de la BD seleccionada al momento de levantar el proyecto, en este archivo tambien se debe configurar las propiedades de la conexión:

```
spring.jpa.hibernate.ddl-auto=create-drop
```

## aplication.propperties:

```
# GENERAL
server.port=8080
#
# DATABASE
spring.datasource.url=jdbc:mysql://localhost/curseexam?useSSL=false
spring.datasource.dbname=curseexam
spring.datasource.username=root
spring.datasource.password=
spring.jpa.hibernate.ddl-auto=create-drop
spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver
```

## PostMan Api Collection

En este repositorio existe un archivo con la coleccion de los endpoints y ejemplos de los Request Bodys JSON para el consumo de la API REST.

Api Test Exams.postman_collection.json

## 1 CREATE STUDENT

El primer endpoint en el flujo es la creación de un estudiante se requiere el siguiente BODY:
```
{
        "name": "Miguel Angel Gonzalez Lopez",
        "age": 31,
        "city": "Bogota",
        "zone": "GMT-6"
}
```
De esta forma se guarda la información solicitada del estudiante 
## 1 CREATE STUDENT RESULTADO ESPERADO

```
{
    "Response": "Registro Correcto",
    "id_alumno": "1"
}
```
Recibimos el ID del estudiante para utilizarlo en los siguientes endpoints.

## 2 CREATE EXAM

El segundo endpoint en el flujo es la creación de un examen se requiere el siguiente BODY:
```
{
  "name": "Primer Parcial",
  "datePresentation":"2023-04-10",
  "totalScore": 100,
  "questions": [
    {
      "text": "¿Cuánto es 2+2?",
      "score": 20,
      "answers": [
        {
          "text": "2",
          "correct": false
        },
        {
          "text": "3",
          "correct": false
        },
        {
          "text": "4",
          "correct": true
        },
        {
          "text": "5",
          "correct": false
        }
      ]
    },
    {
      "text": "¿Cuánto es 3x3?",
      "score": 30,
      "answers": [
        {
          "text": "6",
          "correct": false
        },
        {
          "text": "9",
          "correct": true
        },
        {
          "text": "12",
          "correct": false
        },
        {
          "text": "15",
          "correct": false
        }
      ]
    },
    {
      "text": "¿Cuánto es 10/2?",
      "score": 50,
      "answers": [
        {
          "text": "2",
          "correct": false
        },
        {
          "text": "5",
          "correct": true
        },
        {
          "text": "12",
          "correct": false
        },
        {
          "text": "15",
          "correct": false
        }
      ]
    }
  ]
}
```
De esta forma se guarda la información solicitada del examen, podemos agregar mas preguntas o respuestas segun la nececidad.

## 2 CREATE EXAM RESULTADO ESPERADO

```
{
    "Response": "Examen Creado",
    "respuestas": [
        {
            "id_respuesta": "1",
            "Pregunta": "¿Cuánto es 2+2?",
            "Respuesta": "2"
        },
        {
            "id_respuesta": "2",
            "Pregunta": "¿Cuánto es 2+2?",
            "Respuesta": "3"
        },
        {
            "id_respuesta": "3",
            "Pregunta": "¿Cuánto es 2+2?",
            "Respuesta": "4"
        },
        {
            "id_respuesta": "4",
            "Pregunta": "¿Cuánto es 2+2?",
            "Respuesta": "5"
        },
        {
            "id_respuesta": "5",
            "Pregunta": "¿Cuánto es 3x3?",
            "Respuesta": "6"
        },
        {
            "id_respuesta": "6",
            "Pregunta": "¿Cuánto es 3x3?",
            "Respuesta": "9"
        },
        {
            "id_respuesta": "7",
            "Pregunta": "¿Cuánto es 3x3?",
            "Respuesta": "12"
        },
        {
            "id_respuesta": "8",
            "Pregunta": "¿Cuánto es 3x3?",
            "Respuesta": "15"
        },
        {
            "id_respuesta": "9",
            "Pregunta": "¿Cuánto es 10/2?",
            "Respuesta": "2"
        },
        {
            "id_respuesta": "10",
            "Pregunta": "¿Cuánto es 10/2?",
            "Respuesta": "5"
        },
        {
            "id_respuesta": "11",
            "Pregunta": "¿Cuánto es 10/2?",
            "Respuesta": "12"
        },
        {
            "id_respuesta": "12",
            "Pregunta": "¿Cuánto es 10/2?",
            "Respuesta": "15"
        }
    ],
    "Fecha de Presentacipn": "2023-04-10T00:00:00.000+00:00",
    "id_examen": 1
}

```
Recibimos el ID del examen para utilizarlo en el siguiente endpoint; Así como las preguntas con los identificadores de las respuestas con el fin de enviar las respuestas en el siguiente Endpoint.


## 3 EXAM SOLUTION

El tercer y ultimo endpoint en el flujo es la solucion del examen de estudiante se requiere el siguiente BODY:
```
{
  "exam": 1,
  "student": 1,
  "answerSolution": [3,5,12]
}
```
De esta forma se guarda la información solicitada del estudiante: zona horaria, fecha de presentación y respuestas las cuales es un arreglo de enteros que contienen los id´s de las respuestas seleccionadas en el orden de las preguntas mostradas en la petición pasada.

## 1 EXAM SOLUTION RESULTADO ESPERADO

```
{
    "Response": "Examen Presentado",
    "fecha_presentacion_examen": "2023-04-09 18:00:00.0",
    "fecha_presentacion_alumno": "2023-04-20T12:44:40.100359100-06:00[GMT-06:00]",
    "Zona Horaria": "GMT-6",
    "Calificación": "20",
    "Alumno": "Miguel Angel Gonzalez Lopez",
    "Examen": "Primer Parcial"
}
```
Recibimos la información procesada de la solución del examen con las reglas de negocio solicitadas.

