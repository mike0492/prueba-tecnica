package com.example.ApiExam.controllers;
import com.example.ApiExam.Repository.AnswerRepository;
import com.example.ApiExam.Repository.ExamRepository;
import com.example.ApiExam.Repository.StudentRepository;
import com.example.ApiExam.models.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;

@RestController
public class ExamController {
    @Autowired
    private ExamRepository examRepository;
    @Autowired
    private AnswerRepository answerRepository;

    @Autowired
    private StudentRepository studentRepository;

    @RequestMapping(value = "api/exams",method = RequestMethod.POST)
    public ResponseEntity<Object> createExam(@RequestBody Exam exam) throws JsonProcessingException {
        // validar que las preguntas sumen 100 puntos
        Map<String, Object> data = new HashMap<>();
        if (exam.getQuestions().stream().mapToInt(Question::getScore).sum() != 100) {
            data.put("response", "La suma de los puntajes de las preguntas debe ser 100");
            return new ResponseEntity<>(data, HttpStatus.BAD_REQUEST);
        }
        // guardar el examen y sus preguntas en la base de datos
        List<Question> questions = exam.getQuestions();
        Answer[] answerArray = new Answer[exam.getTotalAnswers()];
        for (int i = 0, inc = 0; i < questions.size(); i++) {
            Question question = questions.get(i);
            question.setExam(exam);
            List<Answer> answers = question.getAnswers();
            for (Answer answer : answers) {
                answer.setQuestion(question);
                answerArray[inc++] = answer;
            }
        }
        examRepository.save(exam);
        data.put("Response", "Examen Creado");
        data.put("id_examen", exam.getId());
        data.put("Fecha de Presentacipn", exam.getDatePresentation());
        //List<String> answerList = new ArrayList<>();
        List<Map<String, Object>> answerList = new ArrayList<>();
        for (Answer answer : answerArray) {
            Map<String, Object> answerMap = new HashMap<>(); // Crea un nuevo mapa para cada respuesta
            answerMap.put("id_respuesta", answer.getId().toString());
            answerMap.put("Respuesta", answer.getText());
            answerMap.put("Pregunta", answer.getQuestion().getText());
            answerList.add(answerMap); // Agrega el mapa a la lista de respuestas

        }
        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(answerList);
        data.put("respuestas", answerList);
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    @RequestMapping(value = "api/exams/solution",method = RequestMethod.POST)
    public ResponseEntity<Object> resolveExam(@RequestBody ExamSolution examsolution) {
        Map<String, String> data = new HashMap<>();
        // guardar el examen y sus preguntas en la base de datos
        List<Integer> answerSolutions = examsolution.getAnswerSolution();
        Student stu = studentRepository.getReferenceById((long) examsolution.getStudent());
        Exam ex = examRepository.getReferenceById((long) examsolution.getExam());
        int score = 0;
        for (int i = 0; i < answerSolutions.size(); i++) {
            Answer answerSolution = answerRepository.getReferenceById(Long.valueOf(answerSolutions.get(i)));
            if(answerSolution.isCorrect())
            {
                score+=answerSolution.getQuestion().getScore();
            }
        }
        data.put("Response", "Examen Presentado");
        data.put("Alumno", stu.getName());
        data.put("Zona Horaria", stu.getZone());
        data.put("fecha_presentacion_alumno", String.valueOf(ZonedDateTime.now(ZoneId.of(stu.getZone()))));
        data.put("Examen", ex.getName());
        data.put("fecha_presentacion_examen", String.valueOf(ex.getDatePresentation()));
        data.put("Calificación",Integer.toString(score));
        return new ResponseEntity<>(data,HttpStatus.OK);
    }
}
