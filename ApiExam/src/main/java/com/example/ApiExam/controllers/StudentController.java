package com.example.ApiExam.controllers;
import com.example.ApiExam.Repository.StudentRepository;
import com.example.ApiExam.models.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;


@RestController
public class StudentController {
    @Autowired
    private StudentRepository studentRepository;
    @PostMapping
    @RequestMapping(value = "api/student",method = RequestMethod.POST)
    public ResponseEntity<Object> createStudent(@RequestBody Student student) {
        Map<String, String> data = new HashMap<>();
        // guardar el alumno y sus datos
        studentRepository.save(student);
        data.put("Response","Registro Correcto");
        data.put("id_alumno", String.valueOf(student.getId()));
        return new ResponseEntity<>(data, HttpStatus.OK);

    }
}
