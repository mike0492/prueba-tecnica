package com.example.ApiExam.models;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

public class ExamSolution {
    @Getter @Setter
    private int exam;

    @Getter @Setter
    private int student;
    @Getter @Setter
    private List<Integer> answerSolution = new ArrayList<>();
}
