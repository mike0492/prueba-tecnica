package com.example.ApiExam.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "exam")
public class Exam  {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter @Setter
    private Long id;
    @Getter @Setter
    private String name;
    @Getter @Setter
    private int totalScore;
    @Getter @Setter
    private Date datePresentation;

    @OneToMany(mappedBy = "exam", cascade = CascadeType.ALL, orphanRemoval = true)
    @Getter @Setter
    private List<Question> questions = new ArrayList<>();

    public int getTotalAnswers() {
        int totalAnswers = 0;
        for (Question question : questions) {
            totalAnswers += question.getAnswers().size();
        }
        return totalAnswers;
    }
}
